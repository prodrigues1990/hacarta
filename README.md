## Enviroment

Create a virtual environment with python 3.7.
```bash
$ virtualenv --python=$(which python3) venv
```

Activate the virtual environment.
```bash
source venv/bin/activate
```

Install application requirements.
```bash
(venv) $ pip install -r requirements.txt
```

## Run the app
```bash
(venv) $ flask run
```

## Theme

https://colorlib.com/wp/template/meal/ license as CC BY 3.0
