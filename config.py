from os import environ

class Config:
    SECRET_KEY = environ.get('SECRET_KEY', 'secret-key')
    MONGO_URI  = environ.get('MONGO_URI', 'mongodb://localhost:27017/acarta')
