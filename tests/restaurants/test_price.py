from unittest import TestCase

from hacarta.restaurants import Price

class TestPrice(TestCase):
    def test_priceMustBeAFloat(self):
        self._assert_error(TypeError, )
        self._assert_error(TypeError, '')
        self._assert_error(TypeError, '1')
        self._assert_error(TypeError, '1.0')
        self._assert_error(TypeError, '.1')
        self._assert_error(TypeError, 3)

    def test_priceCannotBeLowerThanPointOne(self):
        self._assert_error(RuntimeError, .009)
        self._assert_error(RuntimeError, -1.0)
        self._assert_error(RuntimeError, -.01)

    def test_canCreateValidPrice(self):
        price = Price.create(1.0)
        self.assertEqual(price.value, 1.0)

        price = Price.create(1.5)
        self.assertEqual(price.value, 1.5)

        price = Price.create(1.1)
        self.assertEqual(price.value, 1.1)

    def _assert_error(self, error, *params):
        with self.assertRaises(error):
            Price.create(*params)
