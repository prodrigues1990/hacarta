from unittest import TestCase

from hacarta.restaurants import UrlSlug

class TestUrlSlug(TestCase):
    def test_slugMustNotBeEmpty(self):
        with self.assertRaises(TypeError):
            UrlSlug.create()
        with self.assertRaises(RuntimeError):
            UrlSlug.create('')

    def test_slugifiesGivenString(self):
        slug = UrlSlug.create('Given String')
        self.assertEqual(slug.value, 'given-string')
