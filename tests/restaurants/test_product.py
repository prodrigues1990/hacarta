from unittest import TestCase

from hacarta.restaurants.model import Product, Price, Category

class TestProduct(TestCase):
    def setUp(self):
        self.price = Price(10)
        self.category = Category.create('A category')

    def test_productMustHaveANonEmptyName(self):
        with self.assertRaises(RuntimeError):
            Product.create('', '', self.price, [self.category])

    def test_canCreateAValidProduct(self):
        product = Product.create('A Valid Product', '', 10.0, [])
        self.assertEqual(product.name, 'A Valid Product')
        self.assertEqual(product.description, '')
        self.assertEqual(product.price.value, 10.0)
        self.assertEqual(len(product.categories), 0)
        self.assertIs(type(product.id), str)
        self.assertGreater(len(product.id), 0)

        _product = Product.create(
            name='Another valid product',
            description='',
            price=10.5,
            categories=[self.category],
        )
        self.assertEqual(_product.name, 'Another valid product')
        self.assertEqual(_product.description, '')
        self.assertEqual(_product.price.value, 10.5)
        self.assertEqual(len(_product.categories), 1)
        self.assertIs(type(_product.id), str)
        self.assertGreater(len(_product.id), 0)

        self.assertNotEqual(product.id, _product.id)
        self.assertNotEqual(product, _product)
