from unittest import TestCase

from hacarta.restaurants import Category

class TestCategory(TestCase):
    def test_categoryNameMustBeANonEmptyString(self):
        self._assert_raises(TypeError, Category.create, '')
        self._assert_raises(TypeError, Category.create, 1)

    def test_canCreateAValidCategory(self):
        category = Category.create('A Valid Category')
        self.assertEqual(category.name, 'A Valid Category')
        self.assertIs(type(category.id), str)
        self.assertGreater(len(category.id), 0)

        _category = Category.create('Another valid category')
        self.assertEqual(_category.name, 'Another valid category')
        self.assertIs(type(_category.id), str)
        self.assertGreater(len(_category.id), 0)

        self.assertNotEqual(category.id, _category.id)
        self.assertNotEqual(category, _category)

    def _assert_raises(self, error, func, *params):
        with self.assertRaises(error):
            func(*params)
