from dataclasses import dataclass
from uuid import uuid4
import bcrypt

@dataclass(frozen=True)
class UserInfo:
    id: str
    email: str

@dataclass(frozen=True)
class User(UserInfo):
    pwd: str

    @classmethod
    def register(cls, email: str, pwd: str) -> 'User':
        _id  = str(uuid4())
        _pwd = bcrypt.hashpw(str.encode(pwd), bcrypt.gensalt())

        return cls(id=_id, email=email, pwd=_pwd)

    def authenticate(self, pwd: str) -> bool:
        return bcrypt.checkpw(str.encode(pwd), self.pwd)
