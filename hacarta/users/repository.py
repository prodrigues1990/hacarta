from abc import ABC, abstractmethod
from typing import Union

from .model import *

class UserRepository(ABC):
    @abstractmethod
    def save(self, user: User):
        pass

    @abstractmethod
    def  find(self, email: str=None, id: str=None) -> Union[User, None]:
        pass
