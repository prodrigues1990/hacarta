from ...users.repository import *

def get_user(repo: UserRepository, id: str) -> dict:
    user = repo.find(id=id)

    if not user:
        return {'success': False}

    return {'success': True, 'user': {'id': user.id, 'email': user.email}}

def register_user(repo: UserRepository, email: str, pwd: str) -> dict:
    existing = repo.find(email=email)

    if existing:
        return {
            'success': False,
            'error'  : 'User already registered.',
            'fields' : {'email': 'Email address already registered.'},
        }

    user = User.register(email, pwd)
    repo.save(user)

    return {'success': True, 'user': {'id': user.id, 'email': user.email}}

def authenticate_user(repo: UserRepository, email: str, pwd: str) -> dict:
    user = repo.find(email=email)

    if not user:
        return {
            'success': False,
            'error'  : 'User not registered.',
            'fields' : {'email': 'Email address not registered.'}
        }

    if not user.authenticate(pwd):
        return {
            'success': False,
            'error'  : 'Email and password do not match.',
            'fields' : {'pwd': 'Password is not valid.'}
        }

    return {'success': True, 'user': {'id': user.id, 'email': user.email}}
