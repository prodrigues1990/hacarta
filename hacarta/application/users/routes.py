from flask import abort, Blueprint, current_app as app, redirect, render_template, request, url_for
from flask_login import UserMixin, login_required, login_user, logout_user
from urllib.parse import urlparse, urljoin

from .use_cases import *
from .forms import *

bp = Blueprint('users', __name__, template_folder='templates', static_folder='static')
app.login_manager.login_view = 'users.login'
repo = app.repositories['users']

@app.login_manager.user_loader
def load_user(user_id):
    result = get_user(repo, user_id)

    if result['success']:
        return _User.from_dto(result['user'])

@bp.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        email  = form.email.data
        pwd    = form.password.data
        result = authenticate_user(repo, email, pwd)

        if not result['success']:
            if 'email' in result['fields']:
                form.email.errors.append(result['fields']['email'])
            if 'pwd' in result['fields']:
                form.password.errors.append(result['fields']['pwd'])

            return render_template('users/login.html', form=form)

        login_user(_User.from_dto(result['user']))

        next = request.args.get('next')
        if not _is_safe_url(next):
            return abort(400)

        return redirect(next or url_for('restaurants.add'))
    return render_template('users/login.html', form=form)

@bp.route('/signup', methods=['GET', 'POST'])
def signup():
    form = SignupForm()
    if form.validate_on_submit():
        email  = form.email.data
        pwd    = form.password.data
        result = register_user(repo, email, pwd)

        if not result['success']:
            if 'email' in result['fields']:
                form.email.errors.append(result['fields']['email'])
            if 'pwd' in result['fields']:
                form.password.errors.append(result['fields']['pwd'])

            return render_template('users/login.html', form=form)

        login_user(_User.from_dto(result['user']))

        next = request.args.get('next')
        if not _is_safe_url(next):
            return abort(400)

        return redirect(next or url_for('restaurants.add'))
    return render_template('users/signup.html', form=form)

@bp.route('/logout', methods=['GET'])
@login_required
def logout():
    logout_user()

    return redirect(url_for('landing_page'))

def _is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))

    is_http = test_url.scheme in ('http', 'https')
    in_app = ref_url.netloc == test_url.netloc

    return is_http and in_app

class _User(dict, UserMixin):
    def __init__(self, id, email):
        super().__init__()

        self.id   = id    ## so flask login knows
        self.mail = email

    @classmethod
    def from_dto(cls, user_dto):
        return cls(user_dto['id'], user_dto['email'])

    def to_dto(self):
        return {'id': self.id, 'email': self.mail}
