from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, SelectField, FloatField, TextAreaField
from wtforms.validators import DataRequired, Required

class AddRestaurant(FlaskForm):
    name = StringField('Nome', validators=[DataRequired()])
    submit = SubmitField('Adicionar')

class EditRestaurant(FlaskForm):
    name = StringField('Nome', render_kw={'readonly': True})
    slug = StringField('url', render_kw={'readonly': True})
    description = TextAreaField('Descrição')
    footer = TextAreaField('Informação de rodapé')
    submit = SubmitField('Gravar')

    def __init__(self, restaurant):
        super().__init__()

        self.name.data = restaurant['name']
        self.slug.data = restaurant['slug']
        if not self.description.data:
            self.description.data = restaurant['description']
        if not self.footer.data:
            self.footer.data = restaurant['footer']

class AddCategory(FlaskForm):
    name = StringField('Nome', validators=[DataRequired()])
    submit = SubmitField('Adicionar Categoria')

class EditCategory(FlaskForm):
    name = StringField('Nome', validators=[DataRequired()], render_kw={'readonly': True})
    delete = SubmitField('Remover (apaga todos os produtos!)')

    def __init__(self, category):
        super().__init__()

        self.name.data = category['name']

class AddProduct(FlaskForm):
    name = StringField('Nome', validators=[DataRequired()])
    description = TextAreaField('Descrição')
    category = SelectField('Categoria')
    price = FloatField('Preço', validators=[DataRequired()])
    submit = SubmitField('Adicionar Produto')

    def __init__(self, categories):
        super().__init__()

        self.category.choices = [(category, category) for category in categories]

class EditProduct(FlaskForm):
    name = StringField('Nome', validators=[DataRequired()], render_kw={'readonly': True})
    description = TextAreaField('Descrição')
    category = StringField('Categoria', render_kw={'readonly': True})
    price = FloatField('Preço', validators=[DataRequired()])
    submit = SubmitField('Gravar')
    delete = SubmitField('Remover')

    def __init__(self, product, category):
        super().__init__()

        self.name.data = product['name']
        if not self.description.data:
            self.description.data = product['description']
        if not self.category.data:
            self.category.data = category['name']
        if not self.price.data:
            self.price.data = product['price']
