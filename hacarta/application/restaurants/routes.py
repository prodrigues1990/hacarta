from flask import Blueprint, current_app as app, render_template, redirect, request, url_for
from flask_login import current_user as user, login_required

from .use_cases import *
from .forms import *

bp = Blueprint(
    'restaurants',
    __name__,
    template_folder='templates',
    static_folder='static',
)
repo = app.repositories['restaurants']



##
## public views use the restaurant slug to identify a restaurant
##

@app.route('/t/qrcode/<slug>')
@bp.route('/<slug>')
def home(slug):
    return _build_public_view('restaurant', slug)

@bp.route('/<slug>/menu')
def menu(slug):
    return _build_public_view('menu', slug)

def _build_public_view(view_name, slug):
    result = view_restaurant(repo, slug=slug)

    if result['success']:
        view = {'restaurant': result['restaurant']}
        return render_template(f'restaurants/{view_name}.html', **view)

    return redirect(url_for('landing_page'))



##
## authenticated views use the restaurant id and user to identify a restaurant
##

@bp.route('/manage/add', methods=['GET', 'POST'])
@login_required
def add():
    form = AddRestaurant()
    if form.validate_on_submit():
        name    = form.name.data
        result  = register_restaurant(repo, user.id, name)

        if not result['success']:
            if 'name' in result['fields']:
                form.name.errors.append(result['fields']['name'])
                return render_template(
                    'restaurants/add_restaurant.html',
                    form=form
                )
        id = result['restaurant']['id']
        return redirect(url_for('restaurants.edit', restaurant_id=id))

    return render_template('restaurants/add_restaurant.html', form=form)

@bp.route('/manage')
@login_required
def manage():
    result = view_owned_restaurants(repo, user.id)

    return render_template('restaurants/list_restaurants.html', **result)

@bp.route('/manage/<restaurant_id>', methods=['GET', 'POST'])
@login_required
def edit(restaurant_id):
    result = view_owned_restaurant(repo, restaurant_id, user.id)

    if not result['success']:
        return redirect(url_for('restaurants.manage'))

    restaurant = result['restaurant']
    form = EditRestaurant(restaurant)
    if form.validate_on_submit():
        request = {
            'id'            : restaurant_id,
            'description': form.description.data,
            'information': form.footer.data,
        }
        result = edit_restaurant(repo, **request)
        if not result['success']:
            redirect(url_for('restaurants.manage'))

        restaurant = result['restaurant']

    view = {'restaurant': restaurant, 'form': form}
    return render_template('restaurants/edit_restaurant.html', **view)

@bp.route('/manage/<restaurant_id>/categories/add', methods=['GET', 'POST'])
def add_category(restaurant_id):
    result = view_owned_restaurant(repo, restaurant_id, user.id)

    if not result['success']:
        return redirect(url_for('restaurants.manage'))

    form = AddCategory()
    if form.validate_on_submit():
        name   = form.name.data
        result = add_category(repo, restaurant_id, name)

        if result['success']:
            params = {'restaurant_id': restaurant_id}
            return redirect(url_for('restaurants.list_categories', **params))

        if 'name' in result['fields']:
            form.name.errors.append(result['fields']['name'])

    view = {**result, 'form': form}
    return render_template('restaurants/add_edit_category.html', **view)

@bp.route('/manage/<restaurant_id>/categories')
@login_required
def list_categories(restaurant_id):
    result = view_owned_restaurant(repo, restaurant_id, user.id)

    if not result['success']:
        return redirect(url_for('restaurants.manage'))

    return render_template('restaurants/list_categories.html', **result)

@bp.route(
    '/manage/<restaurant_id>/categories/<category_id>',
    methods=['GET', 'POST'],
)
@login_required
def edit_category(restaurant_id, category_id):
    result = view_category(repo, restaurant_id, category_id)

    if not result['success']:
        return redirect(url_for('restaurants.manage'))

    category = result['category']
    form     = EditCategory(category)
    if form.validate_on_submit():
        result = remove_category(repo, restaurant_id, category_id)
        if result['success']:
            return redirect(url_for(
                'restaurants.list_categories',
                restaurant_id=restaurant_id,
            ))

    view = {**result, 'form': form}
    return render_template('restaurants/add_edit_category.html', **view)

@bp.route('/manage/<restaurant_id>/products/add', methods=['GET', 'POST'])
@login_required
def add_product(restaurant_id):
    result = view_owned_restaurant(repo, restaurant_id, user.id)

    if not result['success']:
        return redirect(url_for('restaurants.home'))

    restaurant     = result['restaurant']
    category_names = [category['name'] for category in restaurant['categories']]
    form = AddProduct(category_names)
    if form.validate_on_submit():
        category_dto = [
            c for c in restaurant['categories']
            if c['name'] == form.category.data
        ]
        category_dto = category_dto[0]
        request = {
            'id'         : restaurant_id,
            'name'       : form.name.data,
            'description': form.description.data,
            'price'      : form.price.data,
            'categories' : [category_dto['id']],
        }
        result = add_product(repo, **request)

        if result['success']:
            params = {'restaurant_id': restaurant_id}
            return redirect(url_for('restaurants.list_products', **params))

    view = {**result, 'form': form}
    return render_template('restaurants/add_edit_product.html', **view)

@bp.route('/manage/<restaurant_id>/products')
@login_required
def list_products(restaurant_id):
    result = view_owned_restaurant(repo, restaurant_id, user.id)

    if not result['success']:
        return redirect(url_for('restaurants.manage'))

    return render_template('restaurants/list_products.html', **result)

@bp.route('/manage/<restaurant_id>/products/<product_id>', methods=['GET', 'POST'])
@login_required
def edit_product(restaurant_id, product_id):
    result = view_product(repo, restaurant_id, product_id)

    if not result['success']:
        return redirect(url_for('restaurants.list_products', slug=slug))

    restaurant = result['restaurant']
    product    = result['product']
    category   = product['categories'][0]
    form       = EditProduct(product, category)
    if form.validate_on_submit():
        if form.delete.data:
            result = remove_product(repo, restaurant_id, product_id)
            params = {'restaurant_id': restaurant_id}
            return redirect(url_for('restaurants.list_products', **params))

        if form.submit.data:
            request = {
                'id': restaurant_id,
                'product_id': product_id,
                'name': form.name.data,
                'description': form.description.data,
                'price': float(form.price.data),
                'category_ids': [category['id']],
            }
            result = edit_product(repo, **request)
            params = {'restaurant_id': restaurant_id}
            return redirect(url_for('restaurants.list_products', **params))

    view = {**result, 'form': form}
    return render_template('restaurants/add_edit_product.html', **view)

@bp.route('/manage/<restaurant_id>/qrcodes')
@login_required
def qrcodes(restaurant_id):
    result = view_owned_restaurant(repo, restaurant_id, user.id)

    if not result['success']:
        return redirect(url_for('restaurants.manage'))

    restaurant  = result['restaurant']
    slug        = restaurant['slug']
    public_href = f'{request.host_url}t/qrcode/{slug}'
    view        = {'restaurant': restaurant, 'public_href': public_href}
    return render_template('restaurants/generate_qrcodes.html', **view)
