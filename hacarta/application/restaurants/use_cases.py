from ...restaurants import *

def register_restaurant(repo: RestaurantRepository, user_id: str, name: str):
    user = repo.find_user(user_id)

    try:
        restaurant = Restaurant.register(user, name)
    except RuntimeError as err:
        return {'success': False, 'error': str(err)}

    repo.save(restaurant)

    return {'success': True, 'restaurant': _restaurant_dto(restaurant)}

def view_restaurant(repo: RestaurantRepository, id=None, slug=None):
    restaurant = repo.find(id=id, slug=slug)

    if not restaurant:
        return {'success': False}

    return {'success': True, 'restaurant': _restaurant_dto(restaurant)}

def view_owned_restaurant(repo: RestaurantRepository, id: str, user_id: str):
    user       = repo.find_user(user_id)
    restaurant = repo.find(id=id, user=user)

    return {'success': True, 'restaurant': _restaurant_dto(restaurant)}

def view_owned_restaurants(repo: RestaurantRepository, user_id: str):
    user        = repo.find_user(user_id)
    restaurants = repo.query(user=user)
    restaurants = list(map(_restaurant_dto, restaurants))

    return {'success': True, 'restaurants': restaurants}

def edit_restaurant(
    repo: RestaurantRepository,
    id: str,
    description: str,
    information: str,
):
    restaurant = repo.find(id=id)
    details = RestaurantDetails(
        restaurant.details.name,
        restaurant.details.slug,
        description,
        information
    )
    restaurant = restaurant.set_details(details)
    repo.save(restaurant)

    return {'success': True, 'restaurant': _restaurant_dto(restaurant)}

def add_category(
    repo: RestaurantRepository,
    id: str,
    name: str,
):
    try:
        category = Category.create(name)
    except RuntimeError as err:
        return {'success': False, 'error': str(err)}

    restaurant = repo.find(id=id)
    restaurant = restaurant.add_category(category)
    repo.save(restaurant)

    return {'success': True, 'restaurant': _restaurant_dto(restaurant)}

def view_category(repo: RestaurantRepository, id: str, category_id: str):
    restaurant = repo.find(id=id)
    category   = restaurant.get_category(category_id)

    def category_denormalized(c):
        return _category_denormalized(c, restaurant)

    return {
        'success'   : True,
        'restaurant': _restaurant_dto(restaurant),
        'category'  : category_denormalized(category),
    }

def edit_category(
    repo: RestaurantRepository,
    category_id: str,
    name: str,
):
    restaurant = repo.find(id=id)
    category   = restaurant.get_category(category_id)

    restaurant.remove_category(category)
    restaurant = restaurant.add_category(Category(category.id, name))

    repo.save(restaurant)

    return {'success': True, 'restaurant': _restaurant_dto(restaurant)}

def remove_category(repo: RestaurantRepository, id: str, category_id: str):
    restaurant = repo.find(id=id)
    category = restaurant.get_category(category_id)

    restaurant = restaurant.remove_category(category)

    repo.save(restaurant)

    return {'success': True, 'restaurant': _restaurant_dto(restaurant)}

def add_product(
    repo: RestaurantRepository,
    id: str,
    name: str,
    description: str,
    price: float,
    categories: List[str],
):
    restaurant = repo.find(id=id)
    _categories = [restaurant.get_category(_id) for _id in categories]

    product = Product.create(name, description, price, _categories)
    restaurant = restaurant.add_product(product)

    repo.save(restaurant)

    return {'success': True, 'restaurant': _restaurant_dto(restaurant)}

def view_product(repo: RestaurantRepository, id: str, product_id: str):
    restaurant = repo.find(id=id)
    product    = restaurant.get_product(product_id)

    return {
        'success'   : True,
        'restaurant': _restaurant_dto(restaurant),
        'product'   : _product_denormalized(product),
    }

def edit_product(
    repo: RestaurantRepository,
    id: str,
    product_id: str,
    name: str,
    description: str,
    price: float,
    category_ids: List[str],
):
    restaurant = repo.find(id=id)
    product    = restaurant.get_product(product_id)

    categories = [restaurant.get_category(_id) for _id in category_ids]
    restaurant = restaurant.remove_product(product)
    price      = Price(price)
    product    = Product(
        product.id,
        name=name,
        description=description,
        price=price,
        categories=categories,
    )
    restaurant = restaurant.add_product(product)

    repo.save(restaurant)

    return {'success': True, 'restaurant': _restaurant_dto(restaurant)}

def remove_product(repo: RestaurantRepository, id: str, product_id: str):
    restaurant = repo.find(id=id)
    product = restaurant.get_product(product_id)

    restaurant = restaurant.remove_product(product)

    repo.save(restaurant)

    return {'success': True, 'restaurant': _restaurant_dto(restaurant)}

def _restaurant_dto(restaurant):
    def category_denormalized(c):
        return _category_denormalized(c, restaurant)
    return {
        'id'         : restaurant.id,
        'name'       : restaurant.details.name,
        'slug'       : restaurant.details.slug.value,
        'description': restaurant.details.description,
        'footer'     : restaurant.details.information,
        'categories' : list(map(category_denormalized, restaurant.categories)),
        'products'   : list(map(_product_denormalized, restaurant.products)),
    }

def _category_dto(category):
    return {'id': category.id, 'name': category.name}

def _product_dto(product):
    return {
        'id'         : product.id,
        'name'       : product.name,
        'description': product.description,
        'price'      : product.price.value,
    }


def _category_denormalized(c, restaurant):
    products = list(map(_product_dto, restaurant.get_products(c)))
    return {**_category_dto(c), 'products': products}

def _product_denormalized(p):
    categories = list(map(_category_dto, p.categories))
    return {**_product_dto(p), 'categories': categories}
