from flask import Flask, render_template
from flask_bootstrap import Bootstrap
from flask_login import LoginManager, login_required
from flaskext.markdown import Markdown
from flask_pymongo import PyMongo
from flask_qrcode import QRcode
from flask_simplemde import SimpleMDE
from config import Config

from hacarta.infrastructure.user import UserMongoRepository
from hacarta.infrastructure.restaurant import RestaurantMongoRepository

app = Flask(__name__, static_url_path='/static')
app.config.from_object(Config)

app.login_manager = LoginManager()
app.login_manager.init_app(app)

db = PyMongo(app).db
app.repositories = dict()
user_repo = UserMongoRepository(db)
app.repositories['users'] = user_repo
app.repositories['restaurants'] = RestaurantMongoRepository(user_repo, db)

Bootstrap(app)
Markdown(app)
QRcode(app)
SimpleMDE(app)

@app.route('/t/campaign/1')
@app.route('/')
def landing_page():
    return render_template('new-age-template.html')

with app.app_context():
    from . import users
    from . import restaurants

    app.register_blueprint(users.bp, url_prefix='/users')
    app.register_blueprint(restaurants.bp, url_prefix='/restaurants')
