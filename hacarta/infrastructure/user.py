from hacarta.users import User, UserRepository

class UserMongoRepository(UserRepository):
    def __init__(self, db):
        self.db = db['users']

    def save(self, user):
        db     = self.db
        lookup = {'uid': user.id}
        model  = {'uid': user.id, 'email': user.email, 'pwd': user.pwd}

        existing = db.find_one(lookup)
        if existing:
            db.update_one(lookup, {'$set': model})
        else:
            db.insert_one(model)

    def  find(self, email=None, id=None):
        db = self.db
        if id:
            lookup = {'uid': id}
        elif email:
            lookup = {'email': email}
        else:
            raise RuntimeError()

        model = db.find_one(lookup)

        if model:
            return User(
                id=model['uid'],
                email=model['email'],
                pwd=model['pwd'],
            )
