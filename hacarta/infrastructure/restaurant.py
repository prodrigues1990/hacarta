from hacarta.restaurants.repository import *
from hacarta.users.repository import UserRepository

class RestaurantMongoRepository(RestaurantRepository):
    def __init__(self, user_repo: UserRepository, db):
        self.user_repo = user_repo
        self.db = db['restaurants']

    def save(self, restaurant: Restaurant) -> None:
        db     = self.db
        lookup = {'id': restaurant.id}
        model  = _to_restaurant_model(restaurant)

        existing = db.find_one(lookup)
        if existing:
            db.update_one(lookup, {'$set': model})
        else:
            db.insert_one(model)

    def query(self, user: UserInfo) -> List[Restaurant]:
        lookup = {'owner.id': user.id}
        models = self.db.find(lookup)

        return list(map(_restaurant_entity, models))

    def find(
        self, id: str=None, slug: str=None, user: UserInfo=None
    ) -> Union[Restaurant, None]:
        db = self.db
        if id:
            lookup = {'id': id}
        elif slug:
            lookup = {'details.slug': slug}
        else:
            raise RuntimeError()

        if user:
            lookup['owner.id'] = user.id

        model = db.find_one(lookup)
        if model:
            return _restaurant_entity(model)

    def find_user(self, user_id: str) -> Union[UserInfo, None]:
        return self.user_repo.find(id=user_id)

def _to_restaurant_model(restaurant: Restaurant):
    return {
        'id'        : restaurant.id,
        'owner'     : _user_info_model(restaurant.owner),
        'details'   : _details_model(restaurant.details),
        'categories': list(map(_category_model, restaurant.categories)),
        'products'  : list(map(_product_model, restaurant.products)),
    }

def _user_info_model(user_info: UserInfo):
    return {'id': user_info.id, 'email': user_info.email}

def _details_model(details: RestaurantDetails):
    return {
        'name': details.name,
        'slug': details.slug.value,
        'description': details.description,
        'information': details.information,
    }

def _details_entity(model):
    return RestaurantDetails(
        model['name'],
        UrlSlug(model['slug']),
        model['description'],
        model['information'],
    )

def _slug_model(slug: UrlSlug):
    return slug.slug

def _restaurant_entity(model):
    return Restaurant(
        id         =model['id'],
        owner      =UserInfo(model['owner']['id'], model['owner']['email']),
        details    =_details_entity(model['details']),
        categories =list(map(_category_entity, model['categories'])),
        products   =list(map(_product_entity, model['products'])),
    )

def _category_model(category: Category):
    return {'id': category.id, 'name': category.name}

def _category_entity(model):
    return Category(id=model['id'], name=model['name'])

def _product_model(product: Product):
    return {
        'id'         : product.id,
        'name'       : product.name,
        'description': product.description,
        'price'      : product.price.value,
        'categories' : list(map(_category_model, product.categories)),
    }

def _product_entity(model):
    return Product(
        id         =model['id'],
        name       =model['name'],
        description=model['description'],
        price      =Price(model['price']),
        categories =list(map(_category_entity, model['categories'])),
    )
