from abc import ABC, abstractmethod
from typing import List, Union

from .model import *
from ..users.model import UserInfo

class RestaurantRepository(ABC):
    @abstractmethod
    def save(self, restaurant: Restaurant) -> None:
        pass

    @abstractmethod
    def query(self, user: UserInfo) -> List[Restaurant]:
        pass

    @abstractmethod
    def find(
        self, id: str=None, slug: str=None, user: UserInfo=None
    ) -> Union[Restaurant, None]:
        pass

    @abstractmethod
    def find_user(self, user_id: str) -> Union[UserInfo, None]:
        pass
