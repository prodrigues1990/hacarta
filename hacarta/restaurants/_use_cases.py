from ..users import UserInfo
from .repository import RestaurantRepository
from .restaurant import Restaurant, UrlSlug, Category, Product, Price

def register(
    repo: RestaurantRepository, user_id: str, name: str
) -> dict:
    slug     = UrlSlug.create(name)
    existing = repo.find(slug=slug)
    if existing:
        return {
            'success': False,
            'error': 'Restaurant name already exists.',
            'fields': {'name': 'Name already exists.'},
        }

    restaurant = Restaurant.register_restaurant(name)
    repo.save(user_id, restaurant)

    return {'success': True, 'id': restaurant.id}

def request_one(repo: RestaurantRepository, slug: str) -> dict:
    restaurant = repo.find(slug=UrlSlug(slug))

    return{'success': True, 'restaurant': _to_restaurant_dto(restaurant)}

def request_one_owned_by_user(
    repo: RestaurantRepository, user_id: str, restaurant_id: str
) -> dict:
    restaurant = repo.find(user_id=user_id, restaurant_id=restaurant_id)

    if restaurant:
        return {'success': True, 'restaurant': _to_restaurant_dto(restaurant)}
    return {'success': False, 'error': 'No such restaurant.', 'fields': {}}

def request_all_owned_by_user(
    repo: RestaurantRepository, user_dto: dict
) -> dict:
    user        = UserInfo(id=user_dto['id'], email=user_dto['email'])
    restaurants = repo.query(user=user)
    restaurants = list(map(_to_restaurant_dto, restaurants))

    return {'success': True, 'restaurants': restaurants}

def update(
    repo: RestaurantRepository,
    user_dto: str,
    slug: str,
    description: str=None,
    footer: str=None,
) -> dict:
    user = UserInfo(id=user_dto['id'], email=user_dto['email'])
    restaurant = repo.find(user=user, slug=UrlSlug(slug))

    if not restaurant:
        return {'success': False, 'error': 'No such restaurant.'}

    if description:
        restaurant = restaurant.set_description(description)
    if footer:
        restaurant = restaurant.set_information(footer)

    repo.save(user, restaurant)

    return {'success': True, 'restaurant': _to_restaurant_dto(restaurant)}

def add_category(
    repo: RestaurantRepository,
    restaurant_id: str,
    user_id: str,
    name: str,
) -> dict:
    restaurant = repo.find(user_id=user_id, restaurant_id=restaurant_id)

    for category in restaurant.categories:
        if category.name == name:
            return {
                'success': False,
                'error': 'Category with same name exists.',
                'fields': {'name': 'Name exists.'},
            }

    category   = Category.create(name)
    restaurant = restaurant.set_category(category)
    repo.save(user_id, restaurant)

    return {'success': True, 'restaurant': _to_restaurant_dto(restaurant)}

def request_category(
    repo: RestaurantRepository,
    user_id: dict,
    restaurant_id: str,
    category_id: str,
) -> dict:
    query = {'user_id': user_id, 'restaurant_id': restaurant_id}
    result = request_one_owned_by_user(repo, **query)

    if not result['success']:
        return result

    restaurant = result['restaurant']
    categories = restaurant['categories']
    category   = filter(lambda c: c['id'] == category_id, categories)
    category   = next(category, False)

    if not category:
        return {
            'success': False,
            'error': 'No such category',
            'fields': {'category_id': 'No such category.'},
        }

    return {**result, 'category': category}

def remove_category(
    repo: RestaurantRepository,
    restaurant_id: str,
    user_id: str,
    category_id: str,
) -> dict:
    restaurant = repo.find(user_id=user_id, restaurant_id=restaurant_id)

    if not restaurant:
        return {'success': False, 'error': 'No such restaurant.', 'fields': {}}

    categories = restaurant.categories
    category   = filter(lambda c: c.id == category_id, categories)
    category   = next(category, False)

    if not category:
        return {'success': False, 'error': 'No such category.'}

    restaurant = restaurant.remove_category(category)
    repo.save(user_id, restaurant)
    return {'success': True}

def add_product(
    repo: RestaurantRepository,
    restaurant_id: str,
    user_id: str,
    category_id: str,
    name: str,
    description: str,
    price: float,
) -> dict:
    restaurant = repo.find(user_id=user_id, restaurant_id=restaurant_id)
    categories = restaurant.categories
    category   = filter(lambda c: c.id == category_id, categories)
    category   = next(category, False)

    if not category:
        return {'success': False, 'error': 'No such category.'}

    product = Product.create(
        name=name,
        description=description,
        price=Price.create(price),
    )
    category   = category.add_product(product)
    restaurant = restaurant.set_category(category)
    repo.save(user_id, restaurant)

    return {
        'success'   : True,
        'restaurant': _to_restaurant_dto(restaurant),
        'product'   : _to_product_dto(product),
    }

def request_product(
    repo: RestaurantRepository,
    user_id: str,
    restaurant_id: str,
    product_id: str,
) -> dict:
    restaurant = repo.find(user_id=user_id, restaurant_id=restaurant_id)

    if not restaurant:
        return {'succcess': False, 'error': 'No such product.'}

    for category in restaurant.categories:
        for product in category.products:
            if product.id == product_id:
                return {
                    'success'   : True,
                    'restaurant': _to_restaurant_dto(restaurant),
                    'category'  : _to_category_dto(category),
                    'product'   : _to_product_dto(product)
                }
    return {
        'success': False,
        'error': 'No such product.'
    }

def _to_restaurant_dto(restaurant):
    return {
        'id'         : restaurant.id,
        'name'       : restaurant.name,
        'slug'       : restaurant.slug.slug,
        'description': restaurant.description,
        'footer'     : restaurant.information,
        'categories' : list(map(_to_category_dto, restaurant.categories)),
    }

def _to_category_dto(category):
    return {
        'id'      : category.id,
        'name'    : category.name,
        'products': list(map(_to_product_dto, category.products)),
    }

def _to_product_dto(product):
    return {
        'id'         : product.id,
        'name'       : product.name,
        'description': product.description,
        'price'      : product.price.value,
    }
