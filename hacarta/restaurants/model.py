from dataclasses import dataclass
from typing import List, Union
from uuid import UUID, uuid4
import re

from ..users.model import UserInfo

@dataclass(frozen=True)
class UrlSlug:
    value: str

    @classmethod
    def create(cls, name: str) -> 'UrlSlug':
        slug = re.sub(r'[\W_]+', '-', name.lower())
        if not slug:
            raise RuntimeError('empty slug')
        return cls(slug)

@dataclass(frozen=True)
class Price:
    value: float

    @classmethod
    def create(cls, value: float) -> 'Price':
        if type(value) is not float:
            raise TypeError('price must be a float')
        if not 0.1 < value < 100:
            raise RuntimeError(
                'price must be a decimal number between 0.1 and 100'
            )
        return cls(value)

@dataclass(frozen=True)
class Category:
    id: str
    name: str

    @classmethod
    def create(cls, name: str) -> 'Category':
        if type(name) is not str or not name:
            raise TypeError('name must be a non empty string')
        return cls(id=str(uuid4()), name=name)

@dataclass(frozen=True)
class Product:
    id: str
    name: str
    description: str
    price: Price
    categories: List[Category]

    @classmethod
    def create(
        cls,
        name: str,
        description: str,
        price: float,
        categories: List[Category],
    ) -> 'Product':
        if not name:
            raise RuntimeError('must have a non empty name')
        return cls(
            id=str(uuid4()),
            name= name,
            description=description,
            price=Price(price),
            categories=categories,
        )

@dataclass(frozen=True)
class RestaurantDetails:
    name: str
    slug: UrlSlug
    description: str
    information: str

    @staticmethod
    def create(name: str) -> 'RestaurantDetails':
        slug = UrlSlug.create(name)
        return RestaurantDetails(name, slug, '', '')

@dataclass(frozen=True)
class Restaurant:
    id: str
    owner: UserInfo
    details: RestaurantDetails
    categories: List[Category]
    products: List[Product]

    @staticmethod
    def register(owner: UserInfo, name: str) -> 'Restaurant':
        details = RestaurantDetails.create(name)
        return  Restaurant(
            id=str(uuid4()),
            owner=owner,
            details=details,
            categories=[],
            products=[],
        )

    def set_details(self, details: RestaurantDetails) -> 'Restaurant':
        return Restaurant(
            self.id,
            self.owner,
            details,
            self.categories,
            self.products,
        )

    def add_category(self, category: Category) -> 'Restaurant':
        return Restaurant(
            self.id,
            self.owner,
            self.details,
            self.categories + [category],
            self.products,
        )

    def get_category(self, id: str) -> Union[Category, None]:
        for category in self.categories:
            if category.id == id:
                return category

    def remove_category(self, category: Category) -> 'Restaurant':
        return Restaurant(
            self.id,
            self.owner,
            self.details,
            [c for c in self.categories if c != category],
            self.products,
        )

    def add_product(self, product: Product) -> 'Restaurant':
        return Restaurant(
            self.id,
            self.owner,
            self.details,
            self.categories,
            self.products + [product],
        )

    def get_product(self, id: str) -> Product:
        for product in self.products:
            if product.id == id:
                return product

    def get_products(self, category: Category) -> List[Product]:
        in_category = lambda p: category.id in [c.id for c in p.categories]
        return [p for p in self.products if in_category(p)]

    def remove_product(self, product: Product) -> 'Restaurant':
        return Restaurant(
            self.id,
            self.owner,
            self.details,
            self.categories,
            [p for p in self.products if p != product],
        )
